#include <bits/stdc++.h>

using namespace std;

#include "PersistentSegmentTree.h"

PersistentSegmentTree::PersistentSegmentTree(int n, const long long* numbers) {
    this->numbers = new long long[n];
    for (int i = 0; i < n; i++) {
        this->numbers[i] = numbers[i];
    }

    Node* root = new Node;
    root = build(root, 0, n - 1);
    this->roots.push_back(root);
}

PersistentSegmentTree::~PersistentSegmentTree() {
    for (int i = 0; i < roots.size(); i++) {
        freeMemory(roots[i]);
    }
}

void PersistentSegmentTree::freeMemory(Node* node) {
    if (node->left == nullptr && node->right == nullptr) {
        delete node;
        return;
    }

    freeMemory(node->left);
    freeMemory(node->right);

    delete node;
    return;
}

PersistentSegmentTree::Node* PersistentSegmentTree::build(Node *node, int from, int to) {
    node->from = from;
    node->to = to;
    node->carry = 0;

    if (from == to) {
        node->value = numbers[to];
        return node;
    }
    int mid = (from + to) >> 1;

    node->left = new Node;
    node->left = build(node->left, from, mid);

    node->right = new Node;
    node->right = build(node->right, mid + 1, to);

    node->value = node->left->value + node->right->value;
    return node;
}

PersistentSegmentTree::Node* PersistentSegmentTree::updatePrivate(Node *node, int from, int to, long long value) {
    Node *uptNode = new Node;
    uptNode->from = node->from;
    uptNode->to = node->to;

    if (node->from >= from and node->to <= to) {
        uptNode->value = node->value + (long long)(node->to - node->from + 1) * value;
        uptNode->carry = node->carry + value;
        uptNode->left = node->left;
        uptNode->right = node->right;
        return uptNode;
    }

    uptNode->carry = node->carry;
    if (node->left->to >= from) {
        uptNode->left = updatePrivate(node->left, from, to, value);
    }
    else {
        uptNode->left = node->left;
    }

    if (node->right->from <= to) {
        uptNode->right = updatePrivate(node->right, from, to, value);
    }
    else {
        uptNode->right = node->right;
    }

    long long intervalCarry = (long long)(node->to - node->from + 1) * uptNode->carry;
    uptNode->value = uptNode->left->value + uptNode->right->value + intervalCarry;
    return uptNode;
}

long long PersistentSegmentTree::getSumPrivate(Node *node, int from, int to) {
    if (node->from >= from and node->to <= to) {
        return node->value;
    }

    long long returnValue = 0;
    if (node->left->to >= from) {
        returnValue += getSumPrivate(node->left, from, to);
    }
    if (node->right->from <= to) {
        returnValue += getSumPrivate(node->right, from, to);
    }

    int intervalFrom = max(node->from, from);
    int intervalTo = min(node->to, to);
    return returnValue + (long long)(intervalTo - intervalFrom + 1) * node->carry;
}

void PersistentSegmentTree::update(int from, int to, long long value, int time) {
    Node* newRoot = new Node;
    newRoot = updatePrivate(roots[time], from, to, value);
    roots.push_back(newRoot);
}

long long PersistentSegmentTree::getSum(int from, int to, int time) {
    return getSumPrivate(roots[time], from, to);
}
