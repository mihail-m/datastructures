#include "AhoCorasick.h"

AhoCorasick::AhoCorasick(const vector<string>& words) {
    root = new Node;
    root->init();
    root->par = root;

    for (int i = 0; i < words.size(); i++) {
        addWord(words[i]);
    }

    putLinks();
}

AhoCorasick::~AhoCorasick() {
    freeMemory(root);
}

void AhoCorasick::Node::init()  {
    isWord = false;
    fail = nullptr;
    letter = ' ';
    for (int i = 0; i < MAX_SYMBOLS; i++) {
        child[i] = nullptr;
    }
}

void AhoCorasick::addWord(const string& word) {
    Node* node = root;                              
    for (int i = 0; i < word.size(); i++) {         /// for each letter of the word
        if (node->child[word[i]] == NULL) {         /// if and edge with the letter does not exist
            node->child[word[i]] = new Node;        /// create one
            node->child[word[i]]->init();           /// set initial values
            node->child[word[i]]->par = node;       /// set the parent of the new node
            node->child[word[i]]->letter = word[i]; /// and the letter going into the node
        }
        node = node->child[word[i]];               
    }
    node->isWord = true;                        
}

void AhoCorasick::putLinks() {
    root->fail = root;  /// if we fail at the root we stay there
    queue<Node*> q;     
    q.push(root);

    ///BFS
    while (q.empty() == false) {
        Node* node = q.front();
        q.pop();

        /// we push all children that are reachable whit some letter
        for (int i = 0; i < MAX_SYMBOLS; i++) {
            if (node->child[i] != NULL) {
                q.push(node->child[i]);
            }
        }

        Node* linkTo = node->par->fail; /// we start from the fail link of the parent
        /// we need to find the longest prefix that is a suffix of the word ending in the current node
        /// so we start at the longest possible one -> the one at some child of node at the fail link of the parent

        /// if there is no such child we traverse back trough the links until we find one or we are at the root
        while (linkTo->child[node->letter] == NULL and linkTo != root) {
            linkTo = linkTo->fail;
        }

        node->fail = linkTo->child[node->letter];  /// setting the link

        /// edge cases if there is no prefix that is a suffix of the word ending at the node
        if (node->fail == NULL or node->fail == node) {
            node->fail = root;
        }
    }
}

void AhoCorasick::freeMemory(Node* node) {
    for (int i = 0; i < this->MAX_SYMBOLS; i++) {
        if(node->child[i] != nullptr) {
            freeMemory(node->child[i]);
            node->child[i] = nullptr;
        }
    }

    delete node;
}

void AhoCorasick::checkText(const string& text) {
    Node* node = root;                      
    for (int i = 0; i < text.size(); i++) { 
        Node* n = node;


        while (n->child[text[i]] == NULL and n != root) {
            n = n->fail; /// no edge with the current letter, so we go through the fail links
        }

        /// we found a possible node or we are at the root
        if (n == root) {             /// root case
            n = n->child[text[i]];  /// we continue whit the letter

            /// if we cant we stay at the root
            if (n == NULL) {
                n = root;
            }
        }
        else {
            n = n->child[text[i]]; /// if we found a possible node that is not the root we traverse there
        }

        /// check all the words endings at that index by traversing to the root trough the fail links
        /// if we reach the root trough the fail links of a node all words that end on the current position of the text
        /// will be ending in nodes that the road to the root will contain
        Node* n1 = n;  
        while (n1 != root) {     
            if (n1->isWord) { 
                cout << "Word ending at index: " << i << endl;
            }
            n1 = n1->fail;  /// we go trough the fail link
        }
        /// then we advance to the next node
        node = n;
    }
}

bool AhoCorasick::findWord(const string& word) {
    Node* node = root; 
    for (int i = 0; i < word.size(); i++) {
        if (node->child[word[i]] == NULL) {
            return false;
        }
        node = node->child[word[i]];
    }
    return node->isWord;
}
