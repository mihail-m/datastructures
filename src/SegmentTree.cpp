#include <bits/stdc++.h>

#include "SegmentTree.h"

using namespace std;

template <typename T>
SegmentTree<T>::SegmentTree(int n, const T* treeElements, T (*treeFunction)(T, T)) {
    this->treeFunction = treeFunction;

    this->treeElements = new T[n];
    for (int i = 0; i < n; i++) {
        this->treeElements[i] = treeElements[i];
    }

    this->root = new Node;
    build(root, 0, n - 1);
}

template<typename T>
void SegmentTree<T>::freeMemory(Node* node) {
    if (node->left == nullptr && node->right == nullptr) {
        return;
    }

    freeMemory(node->left);
    freeMemory(node->right);
    
    delete node->left;
    delete node->right;
}

template <typename T>
SegmentTree<T>::~SegmentTree() {
    freeMemory(root);
    delete root;

    delete[] treeElements;
}

template <typename T>
SegmentTree<T>::Node::Node() {
    this->left = nullptr;
    this->right = nullptr;
}

template <typename T>
void SegmentTree<T>::Node::push() {
    value += (to - from + 1) * carry;
    if (left) {
        left->carry += carry;
        right->carry += carry;
    }
    carry = 0;
}

template <typename T>
void SegmentTree<T>::build(Node* node, int from, int to) {
    node->from = from;
    node->to = to;
    node->carry = 0;

    if (from == to) {
        node->value = treeElements[to];
        return;
    }

    node->left = new Node;
    node->right = new Node;

    int middle = (from + to) >> 1;
    build(node->left, from, middle);
    build(node->right, middle + 1, to);

    node->value = treeFunction(node->left->value, node->right->value);
}

template <typename T>
T SegmentTree<T>::executeTreeFunction(Node* node, int from, int to) {
    node->push();
    if (node->from >= from && node->to <= to) {
        return node->value;
    }

    T leftResult;
    bool leftResultCalculated = false;

    T rightResult;
    bool rightResultCalculated = false;

    if (from <= node->left->to) {
        leftResult = executeTreeFunction(node->left, from, to);
        leftResultCalculated = true;
    }

    if (to >= node->right->from) {
        rightResult = executeTreeFunction(node->right, from, to);
        rightResultCalculated = true;
    }

    if (leftResultCalculated && rightResultCalculated) {
        return treeFunction(leftResult, rightResult);
    }
    else if (leftResultCalculated) {
        return leftResult;
    }
    else {
        return rightResult;
    }
}

template <typename T>
void SegmentTree<T>::updateLazy(Node* node, int from, int to, T value) {
    node->push();

    if (node->from > to || node->to < from) {
        return;
    }

    if (node->from >= from && node->to <= to) {
        node->carry += value;
        node->push();
        return;
    }

    updateLazy(node->left, from, to, value);
    updateLazy(node->right, from, to, value);
    node->value = treeFunction(node->left->value, node->right->value);
}

template <typename T>
T SegmentTree<T>::get(int from, int to) {
    return executeTreeFunction(root, from, to);
}

template <typename T>
void SegmentTree<T>::update(int from, int to, T value) {
    updateLazy(root, from, to, value);
}
