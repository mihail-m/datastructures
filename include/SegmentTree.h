#ifndef SEGMENTTREE_H
#define SEGMENTTREE_H


template <typename T>
class SegmentTree {
    private:
        struct Node {
            T value;
            T carry;

            int from;
            int to;

            Node* left;
            Node* right;

            Node();
            void push();
        };

        /// Builds the segment tree O(N*O(treeFunction))
        void build(Node* node, int from, int to);

        void freeMemory(Node* node);

        /// Query function O(log(N)*O(treeFunction))
        T executeTreeFunction(Node* node, int from, int to);

        /// Update function O(log(N)*O(treeFunction))
        void updateLazy(Node* node, int from, int to, T value);

        Node* root;

        T* treeElements;

        /// Function of the tree
        T (*treeFunction)(T, T);

    public:
        SegmentTree(int n, const T* treeElements, T (*treeFunction)(T, T));
        ~SegmentTree();

        void update(int from, int to, T value);
        T get(int from, int to);
};

#endif // SEGMENTTREE_H
