#ifndef AHOCORASICK_H
#define AHOCORASICK_H

#include <bits/stdc++.h>

using namespace std;

class AhoCorasick {
    private:
        static const int MAX_SYMBOLS = 256;

        struct Node {
            bool isWord;
            char letter;
            Node* par;
            Node* fail; /// fail link (a pointer to the node we go to if there is no edge to a child whit the needed letter
            Node* child[MAX_SYMBOLS + 1];

            void init();
        };

        Node* root;

        void addWord(const string& word);

        void putLinks();

        void freeMemory(Node* node);

    public:
        /// Build the automation from a given dictionary
        AhoCorasick(const vector<string>& words);
        ~AhoCorasick();

        /// Given text find all words from the dictionary that the text contains O(length of the text)
        void checkText(const string& text);

        bool findWord(const string& word);
};

#endif // AHOCORASICK_H
