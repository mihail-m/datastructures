#ifndef PERSISTENTSEGMENTTREE_H
#define PERSISTENTSEGMENTTREE_H


class PersistentSegmentTree
{
    private:

        struct Node {
            long long  value;
            long long carry;

            int from;
            int to;

            Node* left;
            Node* right;
        };

        vector<Node*> roots;

        long long* numbers;

        Node* build(Node *node, int from, int to);
        Node* updatePrivate(Node *node, int from, int to, long long value);
        long long getSumPrivate(Node *node, int from, int to);

        void freeMemory(Node* node);


    public:
        PersistentSegmentTree(int n, const long long* numbers);
        ~PersistentSegmentTree();

        void update(int from, int to, long long value, int time);
        long long getSum(int from, int to, int time);
};

#endif // PERSISTENTSEGMENTTREE_H
