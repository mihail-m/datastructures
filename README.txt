The project has the following implementations:
-----------------------------------------------------------------------------

1) Generic segment tree implementation with lazy propagation.
-----------------------------------------------------------------------------
O(N*log(N)) build time.
O(log(N)) update and query time.
-----------------------------------------------------------------------------
O(N) memory.
-----------------------------------------------------------------------------

2) Aho-Corasick implementation for finding word occurrences in a given text.
-----------------------------------------------------------------------------
O(length of the dictionary) build time
O(length of the text) query time
-----------------------------------------------------------------------------
O(length of the dictionary) memory
-----------------------------------------------------------------------------

3) Persistent segment tree with lazy propagation implementation.
-----------------------------------------------------------------------------
O(N*log(N)) build time
O(log(N)) update and query time
-----------------------------------------------------------------------------
O(N * number of updates) memory
-----------------------------------------------------------------------------